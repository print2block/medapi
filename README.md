This is a preli

REST API:
GET: 
	(No authentication required)
	info						- General info (will be provided later)
	record/public/:id			- fetch values of time expirable db entry corresponding to :id, here :id is the _id of the temp object 
	(Authentication by owner org or user required)
	record/private/:id			- fetch values of db entry corresponding to :id, here :id is the phone no
POST: 
	(Authentication by owner Org required)
	info				- update info entry in DB
	(Authentication by any Org required)
	user/add			- Add input data to DB, with input data to be identified by PhoneNo, random password to be generated which is returned along with phone_no
	user/:id			- fetch values of DB  entry  corresponding to :id, here :id is the phone no, the password value should be excluded
	record				- write input data to DB, required input data : phone_no
	(Authentication by either or both  Org and user)
	expose				- copied data of record/private/:id to a time expirable db entry, here :id is the phone_no of user whose all data except password is copied. THis returns _id of the temp object 
	
	
	
	
This problem statement will be updated again.
Recommended tools:
	NODEJS
	MongoDB
	PASSport js for password protection.